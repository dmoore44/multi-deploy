import sys
import json
import requests
import urllib3

#
# main() will be run when you invoke this action
# @param Cloud Functions actions accept a single parameter, which must be a JSON object.
# @return The output of this action, which must be a JSON object.
#
def main(params):
    return call_water(params)

def call_water(params):
   call_response = requests.get('https://tidesandcurrents.noaa.gov/api/datagetter',
       params = {'begin_date': params.get("begin-date"),
              'end_date': params.get("end-date"),
              'product':  params.get("product"),
              'datum': params.get("datum"),
              'interval': params.get("interval"),
              'format': params.get("format"),
              'units': params.get("units"),
              'time_zone': params.get("time-zone"),
              'station': params.get("station")},)

   result_set = call_response.json()

   return result_set

def lambda_handler(event, context):
    return call_water(event)
