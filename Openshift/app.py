from flask import Flask, render_template, request, make_response
import os
import socket
import random
import json
import requests
import urllib3

hostname = socket.gethostname()

app = Flask(__name__)

@app.route("/", methods=['GET'])
def entry_point():
   params = request.get_json()

   call_response = requests.get('https://tidesandcurrents.noaa.gov/api/datagetter',
       params = {'begin_date': params.get("begin-date"),
           'end_date': params.get("end-date"),
           'product':  params.get("product"),
           'datum': params.get("datum"),
           'interval': params.get("interval"),
           'format': params.get("format"),
           'units': params.get("units"),
           'time_zone': params.get("time-zone"),
           'station': params.get("station")},)

   result_set = call_response.json()

   return result_set

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, threaded=True)
